# Kamino Application
A simple react flux application using SWAPI

## Requirements
- NodeJs
- Git

## Installation

```sh
$ yarn install
$ yarn start
```

## Not completed
- Tests
- Progressive web app
- Offline mode