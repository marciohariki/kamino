import {ReduceStore} from 'flux/utils';
import PeopleRepository  from "../API/PeopleRepository";
import PeopleActions from "../actions/PeopleActions";
import PeopleActionTypes from "../constants/PeopleActionsType";
import PeopleDispatcher from "../dispatchers/PeopleDispatcher";
import Immutable from 'immutable';

class PeopleStore extends ReduceStore {
    constructor() {
        super(PeopleDispatcher);
    }

    getInitialState() {
        const state = this.getEmptyState();
        PeopleActions.getPeoplesPage(state.page);
        return state;
    }

    reduce(state, action) {
        switch (action.type) {
            case PeopleActionTypes.REQUEST_PEOPLES_PAGE:
                PeopleRepository.loadPeoples(action.page);
                return Object.assign({}, state, {
                    peoples: [],
                    page: action.page,
                    isLoading: true,
                    error: null
                });

            case PeopleActionTypes.RECEIVE_PEOPLES_PAGE:
                return Object.assign({}, state, {
                    peoples: action.peoplesPage.results,
                    isLoading: false,
                    previousPage: action.peoplesPage.previous,
                    nextPage: action.peoplesPage.next
                });

            case PeopleActionTypes.ERROR_PEOPLES_PAGE:
                return Object.assign({}, state, {
                    peoples: [],
                    isLoading: false,
                    page: action.page,
                    error: action.error
                });

            case PeopleActionTypes.SET_PEOPLE_ACTIVE:
                return Object.assign({}, state, {
                    activePeople: action.people
                });

            case PeopleActionTypes.REMOVE_PEOPLE_ACTIVE:
                return Object.assign({}, state, {
                    activePeople: null
                });

            default:
                return state;

        }
    }

    getEmptyState() {
        return {
            activePeople: null,
            peoples: [],
            isLoading: true,
            page: 1,
            error: null,
            nextPage: null,
            previousPage: null
        }
    }

}

export default new PeopleStore();