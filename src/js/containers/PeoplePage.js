import PeopleListView from '../views/PeopleView';
import {Container} from 'flux/utils';
import PeopleStore from "../stores/PeopleStore";

const getStores = () => {
    return [ PeopleStore ]
};

const getState = () => {
    return { state: PeopleStore.getState() }
};


export default Container.createFunctional(PeopleListView, getStores, getState);