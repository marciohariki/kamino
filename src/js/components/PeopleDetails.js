import React from 'react';
import PropTypes from 'prop-types';
import {Button} from "react-bootstrap";
import PeopleActions from "../actions/PeopleActions";

class PeopleDetails extends React.Component {
    constructor(props) {
        super(props);
    }

    goBack() {
        PeopleActions.removePeopleActive(this.props.people);
    }

    render() {
        if (this.props.isLoading) {
            return <h1>Loading...</h1>
        }
        return (
            <div className="people-details">
                <h1>{this.props.people.name}</h1>
                <ul>
                    <li>Gender: {this.props.people.gender}</li>
                    <li>Height: {this.props.people.height}</li>
                    <li>Homeworld: {this.props.people.homeworld}</li>
                    <li>Mass: {this.props.people.mass}</li>
                    <li>Hair color: {this.props.people.hair_color}</li>
                    <li>Skin color: {this.props.people.skin_color}</li>
                    <li>Eye color:{this.props.people.eye_color}</li>
                    <li>Birth year: {this.props.people.birth_year}</li>
                </ul>
                <Button bsStyle="primary" onClick={() => this.goBack()}>Back</Button>
            </div>
        )

    }
}

PeopleDetails.propTypes = {
    people: PropTypes.object.isRequired,
    isLoading: PropTypes.bool.isRequired
};

export default PeopleDetails;