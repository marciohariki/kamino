import {Table} from "react-bootstrap";
import React from 'react';
import PropTypes from 'prop-types';
import PeopleActions from "../actions/PeopleActions";

class PeopleList extends React.Component {
    constructor(props) {
        super(props);
    }

    getPeopleDetails(people) {
        PeopleActions.setPeopleActive(people);
    }

    render() {
        if (this.props.isLoading) {
            return (
                <h1>Loading...</h1>
            );
        }
        return (
            <div className="people-list">
                <h1>Star wars characters</h1>
                <ul>
                {this.props.peoples.map(people => (
                    <li onClick={() => this.getPeopleDetails(people)}>{people.name}</li>
                ))}
                </ul>
            </div>
        )
    }
}


PeopleList.propTypes = {
    peoples: PropTypes.array.isRequired,
    isLoading: PropTypes.bool.isRequired
};

export default PeopleList;