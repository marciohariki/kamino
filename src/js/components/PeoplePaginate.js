import React from 'react';
import PeopleActions from "../actions/PeopleActions";
import PropTypes from 'prop-types';
import {Button} from "react-bootstrap";

class PeoplePaginate extends React.Component {
    constructor(props) {
        super(props);
    }

    onNext() {
        PeopleActions.getPeoplesPage(this.props.nextPage);
    }


    onPrevious() {
        PeopleActions.getPeoplesPage(this.props.previousPage);
    }

    render() {
        let previousPageButton = (this.props.previousPage) ?
                <Button bsStyle="primary" onClick={() => this.onPrevious()}>Previous</Button> :
                null;

        let nextPageButton = (this.props.nextPage) ?
                <Button bsStyle="primary" onClick={() => this.onNext()}>Next</Button> :
                null;
        return (
            <div className="people-pagination">
                {previousPageButton}
                {nextPageButton}
            </div>
        )

    }
}

PeoplePaginate.propTypes = {
    nextPage: PropTypes.number,
    previousPage: PropTypes.number
}

export default PeoplePaginate;