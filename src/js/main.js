import React from 'react';
import ReactDOM from 'react-dom';
import PeoplesListPage from "./containers/PeoplePage";
import '../stylus/app.styl';

ReactDOM.render(<PeoplesListPage />, document.getElementById('main'));