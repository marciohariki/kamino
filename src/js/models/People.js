import Immutable from 'immutable';

const PeopleRecord = Immutable.Record({
    "name": "",
    "birth_year": "",
    "eye_color": "",
    "gender": "",
    "hair_color": "",
    "height": "",
    "homeworld": "",
    "mass": "",
    "skin_color": "",
    "created": "",
    "edited": "",
    "url": "",
    "films": [],
    "species": [],
    "starships": [],
    "vehicles": []
});

class People extends PeopleRecord {
    static createFromRaw(data) {
        return new People({
            "name": data.name,
            "birth_year": data.birth_year,
            "eye_color": data.eye_color,
            "gender": data.gender,
            "hair_color": data.hair_color,
            "height": data.height,
            "homeworld": data.homeworld,
            "mass": data.mass,
            "skin_color": data.skin_color,
            "created": data.created,
            "edited": data.edited,
            "url": data.url,
            "films": data.films,
            "species": data.species,
            "starships": data.starships,
            "vehicles": data.vehicles
        })
    }
}

export default People;