import Immutable from 'immutable';
import People from "./People";

const PeoplePageRecord = Immutable.Record({
    count: 0,
    next: null,
    previous: null,
    results: []
});

class PeoplesPage extends PeoplePageRecord {
    static createFromRaw(data) {
        return new PeoplesPage({
           count: data.count,
           next: (data.next) ? parseInt(new URL(data.next).searchParams.get("page")) : null,
           previous: (data.previous) ? parseInt(new URL(data.previous).searchParams.get("page")) : null,
           results: data.results.map(rawPeople => People.createFromRaw(rawPeople))
        });
    }
}

export default PeoplesPage;