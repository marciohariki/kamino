import SWAPI from './SWAPI';
import PeopleActions from "../actions/PeopleActions";
import PeoplePage from "../models/PeoplesPage";

class PeopleRepository {
    loadPeoples(page) {
        SWAPI
            .get('/people/', {page: page})
            .then(peoplesPage => PeopleActions.receivedPeoplesPage(PeoplePage.createFromRaw(peoplesPage)))
            .catch(error => PeopleActions.errorPeoplesPage(error));
    }
}

export default new PeopleRepository();