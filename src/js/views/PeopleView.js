import React from 'react';
import PeopleList from "../components/PeopleList";
import PeoplePaginate from "../components/PeoplePaginate";
import PeopleDetails from "../components/PeopleDetails";

const PeopleListView = (props) => {
    return (
        <div>
            <Header/>
            <Body {...props} />
        </div>
    );
}

const Header = () => {
    return (
        <header className="header">
            <h1>Kamino Application</h1>
        </header>
    )
}

const Body = (props) => {
    if (!!props.state.activePeople) {
        return (<PeopleDetails people={props.state.activePeople} />)
    }
    return (
        <div>
            <PeopleList isLoading={props.state.isLoading} peoples={props.state.peoples} />
            <PeoplePaginate nextPage={props.state.nextPage} previousPage={props.state.previousPage}/>
        </div>
    )
}


export default PeopleListView;