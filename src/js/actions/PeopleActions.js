import PeopleDispatcher from "../dispatchers/PeopleDispatcher";
import PeopleActionTypes from "../constants/PeopleActionsType";

const PeopleActions = {
    setPeopleActive(people) {
        PeopleDispatcher.dispatch({
            type: PeopleActionTypes.SET_PEOPLE_ACTIVE,
            people
        })
    },

    removePeopleActive(people) {
        PeopleDispatcher.dispatch({
            type: PeopleActionTypes.REMOVE_PEOPLE_ACTIVE,
            people
        })
    },

    getPeoplesPage(page) {
        PeopleDispatcher.dispatch({
            type: PeopleActionTypes.REQUEST_PEOPLES_PAGE,
            page
        })
    },
    receivedPeoplesPage(peoplesPage) {
        PeopleDispatcher.dispatch({
            type: PeopleActionTypes.RECEIVE_PEOPLES_PAGE,
            peoplesPage
        })
    },
    errorPeoplesPage(page, error) {
        PeopleDispatcher.dispatch({
            type: PeopleActionTypes.ERROR_PEOPLES_PAGE,
            page,
            error
        });
    },


};

export default PeopleActions;